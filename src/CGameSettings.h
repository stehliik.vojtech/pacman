/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#ifndef PACMAN_CGAMESETTINGS_H
#define PACMAN_CGAMESETTINGS_H

#include "CMazeSettings.h"

class CGameSettings {
public:
	int spiritSpeed = 20;
	int playerSpeed = 20;

	CMazeSettings mazeSettings;

	CGameSettings(int spiritSpeed, int playerSpeed, const CMazeSettings& mazeSettings);

	static CGameSettings fromCache();
};

#endif //PACMAN_CGAMESETTINGS_H

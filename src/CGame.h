/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#ifndef PACMAN_CGAME_H
#define PACMAN_CGAME_H

#include "CMaze.h"
#include "CGameSettings.h"

class CGame {
	CGameSettings settings;

	CMaze maze;

public:
	const size_t SPIRIT_COUNT = 5;

	CGame(const CGameSettings &settings);
};


#endif //PACMAN_CGAME_H

/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#ifndef PACMAN_CSPIRITOIKAKE_H
#define PACMAN_CSPIRITOIKAKE_H

#include "CSpirit.h"

/**
 * Red Spirit (aggresive)
 */
class CSpiritOikake : public CSpirit {

};


#endif //PACMAN_CSPIRITOIKAKE_H

#include <iostream>
#include <ncurses.h>
#include "CUserInterface.h"

constexpr const char* PACMAN_VERSION = "0.0.1";

void initializeNcurses();
void destructNcurses();

int main() {
	initializeNcurses();

	CUserInterface ui;
	ui.showMenuWindow();

	destructNcurses();
	return 0;
}


void initializeNcurses() {
	// Initializes ncurses library
	initscr();
	// Disables line buffering of input
	cbreak();
	// Disables echoing of input
	noecho();
	// Enables special keys such as arrows
	keypad(stdscr, TRUE);
}

void destructNcurses() {
	endwin();
}

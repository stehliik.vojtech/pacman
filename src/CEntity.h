/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#ifndef PACMAN_CENTITY_H
#define PACMAN_CENTITY_H

#include "CCoord.h"
#include "EDirection.h"
#include "CMaze.h"

class CEntity {
public:
	explicit CEntity(const CMaze &maze);

	virtual void tick() final;

	[[nodiscard]] bool isAlive() const;
protected:
	const CMaze &maze;

	bool alive = true;

	CCoord* position = nullptr;
	EDirection direction = EDirection::DOWN;

	virtual void doTick() = 0;
};

#endif //PACMAN_CENTITY_H

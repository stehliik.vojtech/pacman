/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#include "CMaze.h"

CMaze::CMaze(size_t x, size_t y)
	: size(CCoord(x, y)) {}

CCoord CMaze::getSize() const {
	return size;
}

CMaze::~CMaze() = default;

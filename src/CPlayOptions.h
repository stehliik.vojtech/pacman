/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 07.05.2023
 */

#ifndef PACMAN_CPLAYOPTIONS_H
#define PACMAN_CPLAYOPTIONS_H

#include <string>

/**
 * Stores User-Defined options, mostly configured in the UI before the
 * game starts.
 */
class CPlayOptions {
public:
	CPlayOptions();

	std::string nickname;
	int mapId;
};

#endif //PACMAN_CPLAYOPTIONS_H

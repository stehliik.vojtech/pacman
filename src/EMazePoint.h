/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#ifndef PACMAN_EMAZEPOINT_H
#define PACMAN_EMAZEPOINT_H

enum EMazePoint : char {
	NONE = 0,
	WALL = 1
};

#endif //PACMAN_EMAZEPOINT_H

/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#ifndef PACMAN_EDIRECTION_H
#define PACMAN_EDIRECTION_H

/**
 * Entity directions
 * - Right turn could be done by (d + 1) & 3
 * - 180 degree turn could be done by (d + 2) & 3
 * - Left turn could be done by (d + 3) & 3
 */
enum class EDirection {
	UP = 0,
	RIGHT = 1,
	DOWN = 2,
	LEFT = 3
};

#endif //PACMAN_EDIRECTION_H

/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#include "CGame.h"

CGame::CGame(const CGameSettings &settings)
	: maze(CMaze(settings.mazeSettings.getSizeX(), settings.mazeSettings.getSizeY())), settings(settings) {}

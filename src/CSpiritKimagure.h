/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#ifndef PACMAN_CSPIRITKIMAGURE_H
#define PACMAN_CSPIRITKIMAGURE_H

#include "CSpirit.h"

/**
 * Aqua Spirit (unpredictable)
 */
class CSpiritKimagure : public CSpirit {

};


#endif //PACMAN_CSPIRITKIMAGURE_H

/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#ifndef PACMAN_CMAZESETTINGS_H
#define PACMAN_CMAZESETTINGS_H

#include <cstddef>
#include <string>
#include "CCoord.h"
#include "CMaze.h"

class CMazeSettings {
	size_t sizeX, sizeY;

	CCoord* barriers;

	CCoord startPos;
	CCoord* spiritStatPos;

public:
	CMazeSettings(size_t sizeX, size_t sizeY, CCoord barriers[], CCoord startPos, CCoord spiritStartPos[5]);
	~CMazeSettings();

	[[nodiscard]] size_t getSizeX() const;
	size_t getSizeY() const;

	[[nodiscard]] CCoord* getBarriers() const;

	[[nodiscard]] CCoord getStartPos() const;

	[[nodiscard]] CCoord* getSpiritStartPos() const;

	/**
	 * Returns basic maze, generaly made for testing
	 */
	static CMazeSettings fromCache();

	/**
	 * Loads CMazeSettings from file. Throws exception if the file is corrupted, or any
	 * other problem occurs
	 */
	static CMazeSettings fromFile(const std::string &sourceFile);

	/**
	 * Generates random CMazeSettings from seed
	 */
	static CMazeSettings fromRandom(int seed);
};

#endif //PACMAN_CMAZESETTINGS_H

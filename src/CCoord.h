/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#ifndef PACMAN_CCOORD_H
#define PACMAN_CCOORD_H

#include <cstdio>

class CCoord {
	size_t x, y;
public:
	CCoord(size_t x, size_t y);
	CCoord(const CCoord &coord);

	CCoord& operator = (const CCoord &coord);

	[[nodiscard]] CCoord add(size_t x, size_t y) const;

	[[nodiscard]] CCoord subtract(size_t x, size_t y) const;

	[[nodiscard]] CCoord addCoord(const CCoord &coord) const;

	[[nodiscard]] CCoord subtractCoord(const CCoord &coord) const;

	[[nodiscard]] CCoord multiply(size_t multiplier) const;
};

#endif //PACMAN_CCOORD_H

/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#ifndef PACMAN_CSPIRITMATCHIBUSE_H
#define PACMAN_CSPIRITMATCHIBUSE_H

#include "CSpirit.h"

/**
 * Pink Spirit (astute)
 */
class CSpiritMatchibuse : public CSpirit {

};


#endif //PACMAN_CSPIRITMATCHIBUSE_H

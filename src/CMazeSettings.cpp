/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#include "CMazeSettings.h"

CMazeSettings::CMazeSettings(size_t sizeX, size_t sizeY, CCoord barriers[], CCoord startPos, CCoord spiritStartPos[])
	: sizeX(sizeX), sizeY(sizeY), startPos(startPos) {
	// TODO
}

CMazeSettings::~CMazeSettings() {
	// TODO
}

CMazeSettings CMazeSettings::fromCache() {
	return {20, 10, {}, {(size_t)0, (size_t)0}, {}}; // TODO
}

CMazeSettings CMazeSettings::fromFile(const std::string &sourceFile) {
	return CMazeSettings::fromCache(); // TODO
}

CMazeSettings CMazeSettings::fromRandom(int seed) {
	return CMazeSettings::fromCache(); // TODO
}

size_t CMazeSettings::getSizeX() const {
	return sizeX;
}

size_t CMazeSettings::getSizeY() const {
	return sizeY;
}

CCoord *CMazeSettings::getBarriers() const {
	return barriers;
}

CCoord CMazeSettings::getStartPos() const {
	return startPos;
}

CCoord* CMazeSettings::getSpiritStartPos() const {
	return spiritStatPos;
}

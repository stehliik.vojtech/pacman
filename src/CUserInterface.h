/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 07.05.2023
 */

#ifndef PACMAN_CUSERINTERFACE_H
#define PACMAN_CUSERINTERFACE_H

#include <string>
#include <map>
#include <functional>
#include "CPlayOptions.h"

class CUserInterface {
	enum EMenu {
		MAIN = 0,
		PLAY = 1,
		OPTIONS = 2
	};

	struct CMenu {
		std::function<void(size_t)> handler;
		std::vector<std::string> options;

		CMenu(const std::function<void(size_t)> &handler, const std::vector<std::string>& options);
	};

	bool open = false;
	EMenu currentMenu = MAIN;

	CPlayOptions playOptions;

	void handleMainMenuClick(size_t button);

	void handlePlayMenuClick(size_t button);

	std::map<EMenu, CMenu> menus = {
		std::pair<EMenu, CMenu>{MAIN, {
			[this](size_t button) -> void {handleMainMenuClick(button);},
			{"Play", "Configurations", "Quit"}
		}},
		std::pair<EMenu, CMenu>{PLAY, {
			[this](size_t button) -> void {handlePlayMenuClick(button);},
			{"Start Game", "Configuration: 0 (Default)", "Nickname: " + playOptions.nickname, "Back to Main Menu"}
		}}
	};
public:
	CUserInterface();

	void showMenuWindow();
};

#endif //PACMAN_CUSERINTERFACE_H

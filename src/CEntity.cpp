/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#include "CEntity.h"

CEntity::CEntity(const CMaze &maze): maze(maze) {}

void CEntity::tick() {
	if(alive) {
		doTick();
	}
}

bool CEntity::isAlive() const {
	return alive;
}


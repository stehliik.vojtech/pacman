/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#ifndef PACMAN_CMAZE_H
#define PACMAN_CMAZE_H

#include <cstddef>
#include "CCoord.h"
#include "EMazePoint.h"

class CMaze {
	CCoord size;

	EMazePoint *mapData = nullptr;
public:
	CMaze(size_t x, size_t y);
	~CMaze();

	[[nodiscard]] CCoord getSize() const;
};


#endif //PACMAN_CMAZE_H

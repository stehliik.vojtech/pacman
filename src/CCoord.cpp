/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#include "CCoord.h"

CCoord::CCoord(size_t x, size_t y): x(x), y(y) {}

CCoord::CCoord(const CCoord &coord) = default;

CCoord& CCoord::operator = (const CCoord &coord) {
	x = coord.x;
	y = coord.y;
	return *this;
}

CCoord CCoord::add(size_t x, size_t y) const {
	return {this->x + x, this->y + y};
}

CCoord CCoord::subtract(size_t x, size_t y) const {
	return {this->x - x, this->y - y};
}

CCoord CCoord::addCoord(const CCoord &coord) const {
	return {this->x + coord.x, this->y + coord.y};
}

CCoord CCoord::subtractCoord(const CCoord &coord) const {
	return {this->x - coord.x, this->y - coord.y};
}

CCoord CCoord::multiply(size_t multiplier) const {
	return {this->x * multiplier, this->y * multiplier};
}

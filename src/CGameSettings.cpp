/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 06.05.2023
 */

#include "CGameSettings.h"

CGameSettings::CGameSettings(int spiritSpeed, int playerSpeed, const CMazeSettings &mazeSettings)
	: spiritSpeed(spiritSpeed), playerSpeed(playerSpeed), mazeSettings(mazeSettings) {}

CGameSettings CGameSettings::fromCache() {
	return {20, 20, CMazeSettings::fromCache()};
}

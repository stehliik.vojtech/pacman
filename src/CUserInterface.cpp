/**
 * @author Vojtěch Stehlík <stehlvo2@fit.cvut.cz>
 * @date 07.05.2023
 */

#include "CUserInterface.h"
#include <ncurses.h>
#include <utility>

CUserInterface::CMenu::CMenu(const std::function<void(size_t)> &handler, const std::vector<std::string> &options)
	: handler(handler), options(options) {}

void CUserInterface::handleMainMenuClick(size_t button) {
	if(button == 0) {
		currentMenu = PLAY;
	} else if(button == 1) {

	} else if(button == 2) {
		open = false;
	}
}

void CUserInterface::handlePlayMenuClick(size_t button) {
	if(button == 3) {
		currentMenu = MAIN;
	}
}

CUserInterface::CUserInterface() = default;

void CUserInterface::showMenuWindow() {
	open = true;

	currentMenu = EMenu::MAIN;
	EMenu prevMenu = EMenu::MAIN;

	size_t chosen = 0;
	size_t optCount;

	std::function<void(size_t)> handler = menus.at(currentMenu).handler;

	while(open) {
		optCount = menus.at(currentMenu).options.size();
		if(prevMenu != currentMenu) {
			chosen = 0;
			handler = menus.at(currentMenu).handler;
		}
		prevMenu = currentMenu;

		// Printing menu options
		clear();
		for(size_t i = 0; i < optCount; ++i) {
			if(i == chosen) {
				attron(A_REVERSE);
			} else {
				attroff(A_REVERSE);
			}

			mvprintw((int)i + 1, 1, "%s", menus.at(currentMenu).options[i].c_str());
		}

		// Handling input
		int ch = getch();
		if(ch == KEY_UP) {
			chosen = (chosen + optCount - 1) % optCount;
		} else if(ch == KEY_DOWN) {
			chosen = (chosen + 1) % optCount;
		} else if(ch == KEY_ENTER || ch == '\n') {
			handler(chosen);
		}
	}
}
